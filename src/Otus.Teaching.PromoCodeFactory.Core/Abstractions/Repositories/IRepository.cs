﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<ICollection<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<T> CreateAsync(T entity);

        Task<T> UpdateByIdAsync(Guid id);

        Task<string> DeleteByIdAsync(Guid id);
    }
}