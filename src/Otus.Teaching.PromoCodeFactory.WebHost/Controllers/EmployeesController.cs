﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpGet("create/{name}/{email}/{lastname}/{appliedPromocodesCount:int}")]
        public async Task<ActionResult<Employee>> CreateAsync(string name, string email, string lastname, int appliedPromocodesCount)
        {
            Employee employee = await _employeeRepository.CreateAsync(
                new Employee {
                    Id = Guid.NewGuid(),
                    Email = email,
                    FirstName = name,
                    LastName = lastname,
                    Roles = new List<Role>(),
                    AppliedPromocodesCount = appliedPromocodesCount
                }
            );

            return employee;
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpGet("delete/{id:guid}")]
        public async Task<ActionResult<string>> DeleteAsync(Guid id)
        {
            return await _employeeRepository.DeleteByIdAsync(id);
        }

        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpGet("update/{id:guid}/{name}/{email}/{lastname}/{appliedPromocodesCount:int}")]
        public async Task<ActionResult<Employee>> UpdateAsync(Guid id,
                                                              string name,
                                                              string email,
                                                              string lastname,
                                                              int appliedPromocodesCount)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            employee.FirstName = name;
            employee.LastName = lastname;
            employee.Email = email;
            employee.AppliedPromocodesCount = appliedPromocodesCount;

            string rzt = await _employeeRepository.DeleteByIdAsync(id);

            Employee employee_rzt = await _employeeRepository.CreateAsync(employee);

            return employee;
        }
    }
}