﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(IList<T> data)
        {
            Data = data;
        }
        
        public Task<ICollection<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T entity)
        {
            Data.Add(entity);
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == entity.Id));
        }

        

        public Task<string> DeleteByIdAsync(Guid id)
        {
            try
            {
                Data.Remove(Data.FirstOrDefault(x => x.Id == id));
                return Task.FromResult("deleted");
            }
            catch (Exception ex)
            {
                return Task.FromResult(ex.Message.ToString());
            }
        }

        public Task<T> UpdateByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}